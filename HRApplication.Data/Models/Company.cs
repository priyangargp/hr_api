﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRApplication.Data.Models
{
    public class Company
    {
        public Group Group { get; set; }

        [Key]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Address_1 { get; set; }

        public string Address_2 { get; set; }

        public string Address_3 { get; set; }

        [Required]
        public string ContactNo { get; set; }

        [Required]
        public int Status { get; set; }

        [Required]
        public DateTime CreatedOn { get; set; }

        [Required]
        public string CreatedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public string ModifiedBy { get; set; }
    }
}
