﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Web;
using HRApplication.Data.Models;

namespace HRApplication.Data.DBContext
{
    public class HRDBContext: DbContext
    {
        public HRDBContext()
            : base("")
        { }

        public DbSet<Company> Companies { get; set; }
        public DbSet<Group> Groups { get; set; }


    }
}
